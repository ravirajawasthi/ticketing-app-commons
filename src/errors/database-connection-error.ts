import { CustomError } from './custom-error';

export class DatabaseConnectionError extends CustomError {
  statusCode = 500;
  reason = 'Database Down!';
  constructor() {
    super('Error connecting to database');
    //Required because we are extending a built in class from js
    Object.setPrototypeOf(this, DatabaseConnectionError.prototype);
  }
  serializeError() {
    return [{ message: this.reason }];
  }
}
